import enum
import urllib.parse
import bs4
import logging


class Type(enum.Enum):
    """ Class enumerating the different types of e-boks folders """

    # Inbox folder where all incoming mail is (by default) located.
    inbox = 'B'

    # Drafts folder where all unsent message drafts are located.
    drafts = 'K'

    # Folder where all deleted messages are located. Messages in here are permanently deleted after 30 days.
    deleted_items = 'P'

    # Folder where all sent messages are located.
    sent_items = 'S'

    # Custom folder that has been created by the user.
    custom_folder = 'O'

    # Custom folder that has been created by the user. This folder in located inside a custom folder or subfolder.
    # It can only be inside a custom folder or subfolder, not inside inbox, drafts, etc.
    custom_subfolder = 'U'


class Folder:
    """ Class describing an e-boks folder """
    def __init__(self):

        # Unique folder identification number. The id cannot change and persists for the lifetime of the folder.
        self.id = ''

        # The folder name.
        self.name = ''

        # The folder type. Can be one of the enumerations in the eboks.folder.Type class.
        self.type = None

        # The amount of unread messages in the folder.
        self.messages_unread = 0

        # The parent folder id number. If a folder does not have a parent, meaning that the folder is a top level
        # folder, this parent folder id will be '0'. Parent folder id '0' represents the root folder.
        self.parent_folder_id = ''

        # List of folder id numbers for the folders inside this folder.
        self.sub_folder_ids = []


def get_folders(session):
    """ Get a list of all folders in an e-boks account.

    This will return one list mixed with both folders and subfolders. Each folder has an id for its parent folder
    and a list of ids for its subfolders. """
    url = '{uri}/0/mail/folders'.format(uri=session.uri)
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')

    # Convert the response body to a list of folders.
    folders = []
    for folder_xml in body.find('folders'):
        # The parent to these top level folders is the root folder with id '0'.
        append_folder_from_xml(session, folders, folder_xml, '0')
    return folders


def append_folder_from_xml(session, folders, folder_xml, parent_folder_id):
    """ Convert an e-boks folder xml response to one or more Folder instance. Appends the folder and its subfolders
    to a list recusively. """
    folder = Folder()
    folder.id = folder_xml['id']
    folder.name = folder_xml['name']
    folder.type = Type(folder_xml['type'])
    folder.messages_unread = int(folder_xml['unread'])
    folder.parent_folder_id = parent_folder_id
    folders.append(folder)

    # A folder can have several subfolders, we recursively decode these.
    for subfolder_xml in folder_xml:
        subfolder = append_folder_from_xml(session, folders, subfolder_xml, folder.id)
        folder.sub_folder_ids.append(subfolder.id)
    return folder


def get_message_total(session, folder):
    """ Get the total amount of messages in a given folder. """
    url = '{uri}/0/mail/folder/{folderid}?skip=0&take=0'.format(
        uri=session.uri,
        folderid=folder.id
    )
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')
    return int(body.folder['messages'])


def create(session, name, parent_folder=None):
    """ Create an e-boks folder.

    If you want to create a subfolder, pass in the parent folder. The parent folder must be a custom folder or
    custom subfolder.

    :return: A Folder instance with the created folder.
    """
    parent_folder_id = (parent_folder.id if parent_folder else '0')
    folder_type = (Type.custom_subfolder if parent_folder else Type.custom_folder)

    url = '{uri}/0/mail/folder/{parent_folder_id}?folderName={name}'.format(
        uri=session.uri,
        parent_folder_id=parent_folder_id,
        name=urllib.parse.quote(name)
    )
    session.put(url)

    # We do not know the new folder's id. To find it we search though all the e-boks folders.
    folder = find_by_name(session, name, folder_type, parent_folder)
    logging.info('Created folder with name ' + folder.name + ' and id ' + folder.id + '.')
    return folder


def delete(session, folder):
    """ Delete an e-boks folder.

    It is only possible to delete custom folders/subfolders and the folder has to be empty.
    """
    if folder.type != Type.custom_folder and folder.type != Type.custom_subfolder:
        raise ValueError('It is only possible to delete custom folders/subfolders')
    if get_message_total(session, folder) != 0:
        raise ValueError('It is only possible to delete empty folder.')
    url = '{uri}/0/mail/folder/{folder_id}'.format(uri=session.uri, folder_id=folder.id)
    session.delete(url)
    logging.info('Deleted folder with name ' + folder.name + ' and id ' + folder.id + '.')


def rename(session, folder, newname):
    """ Rename an e-boks folder.

    It is only possible to rename custom folders/subfolders.
    """
    if folder.type != Type.custom_folder and folder.type != Type.custom_subfolder:
        raise ValueError('It is only possible to rename custom folders/subfolders.')
    url = '{uri}/0/mail/folder/{folder_id}?newFolderName={newname}&toFolderID={tofolderid}'.format(
        uri=session.uri,
        folder_id=folder.id,
        newname=urllib.parse.quote(newname),
        tofolderid=folder.parent_folder_id
    )
    session.put(url)


def move(session, folder, destination_folder):
    """ Move an e-boks folder.

    It is only possible to move custom folders/subfolders. Set destination folder to None if you want the folder
    to be a top level folder and not a subfolder.
    """
    if folder.type != Type.custom_folder and folder.type != Type.custom_subfolder:
        raise ValueError('It is only possible to move custom folders/subfolders.')
    destination_folder_id = destination_folder.id if destination_folder else '0'
    url = '{uri}/0/mail/folder/{folder_id}?newFolderName={newname}&toFolderID={tofolderid}'.format(
        uri=session.uri,
        folder_id=folder.id,
        newname=folder.name,
        tofolderid=destination_folder_id
    )
    session.put(url)


def find_by_name(session, name, folder_type, parent_folder=None):
    """ Find an e-boks folder based on its name, parent and type.

    If you do not have a folders id, then the only way to uniquely identify that folder is by giving its name, parent
    and type. This is only possible, beacuse it is illegal to create two custom folders, with the same name in the
    same folder.
    """
    parent_folder_id = parent_folder.id if parent_folder else '0'
    for folder in get_folders(session):
        if folder.name == name and folder.parent_folder_id == parent_folder_id and folder.type == folder_type:
            return folder
    raise LookupError('Could not find folder ' + name)


def find_by_id(session, folder_id):
    """ Find an e-boks folder from its id. """
    for folder in get_folders(session):
        if folder.id == folder_id:
            return folder
    raise LookupError('Could not find folder with id ' + folder_id)
