import logging
import bs4
import enum


class Customer:
    """ A customer is someone that is registered on e-boks as a potential sender.

    You have to register with the customer before you can receive mail from them. Customer and sender ids are not
    the same! Once you register with a customer and they send you mail, they get a sender id. Customers can be both
    government bodies and private companies.
    """
    def __init__(self):

        # Unique customer identification number. The id cannot change.
        self.id = ''

        # Customer name.
        self.name = ''

        # Url for a customer logo.
        self.logo_url = ''

        # Have you registered to received mail from this customer?
        self.is_subscribed = False


class Group:
    """ Each customer have one or more groupings of mail that they can send you.

    You can choose to receive mail from select groups, by subscribing to these. If you subscribe to a customer then
    you effectively subscribe to all groups that the customer have.

    For example a bank can have two groups. One for bank statements and another for everything else. Then you
    can subscribe to only the bank statements, if you only want those over e-boks.
    """
    def __init__(self):

        # The group name.
        self.name = ''

        # The group id.
        self.id = ''

        # Have you registered to received mail from this group?
        self.is_subscribed = False


class Category:
    """ Customers are grouped into categories.

    This allows you to for example get a list of all customers that deals with realestate. There are many
    categories like banks, municiplacities, state, insurance, etc.
    """
    def __init__(self):

        # Category identification number.
        self.id = ''

        # Category name.
        self.name = ''

        # The amount of customers in the category.
        self.customer_amount = 0

        # The segment that the category belongs to. Each category is reserved for either government entities or
        # private companies, there cannot be customers from both in the same category.
        self.segment = None


class Segment(enum.Enum):
    """ Enumeration describing the segments that customers/categories are grouped into. """

    # Public segment for government entities.
    public = 'public'

    # Private segment for private companies.
    private = 'private'


def get_groups(session, customer):
    """ Get the groups that a customer has divided its mail into. Returns a list of Group instances.

    You can individually subscribe to these if you do not want all mail from the customer.
    """
    url = '{uri}/0/groups/customer/{customer_id}'.format(uri=session.uri, customer_id=customer.id)
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')

    groups = []
    for groups_xml in body.find('subscrpgroups'):
        group = Group()
        group.id = groups_xml['subscrpid']
        group.name = groups_xml['name']
        group.is_registered = int_string_to_bool(groups_xml['subscription'])
        groups.append(group)
    return groups


def get_group_description(session, group):
    """ Get a text description for a customers group. """
    url = '{uri}/0/groups/subscrpgroup/{subscriber_group_id}'.format(
        uri=session.uri,
        subscriber_group_id=group.id
    )
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')
    return body.find('description').string


def get_information(session, customer):
    """ Get information about a customer.

    Returns an xml structure with name, address, description, webaddress , etc.. Usually these are very poorly
    filled out. Almost unusable.
    """
    url = '{uri}/0/groups/customer/{customer_id}/information'.format(uri=session.uri, customer_id=customer.id)
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')
    return body.information


def subscribe(session, customer_or_group):
    """ Subscribe to receive mail from either a customer or from one of its groups.

    By subscribing to a customer, you effectively subscribe to all its groups of mail.
    """
    set_subscription(session, customer_or_group.id, True)
    customer_or_group.is_subscribed = True


def unsubscribe(session, customer_or_group):
    """ Unsubscribe from receiving mail from a customer or from one of its groups.

    By unsubscribing from a customer, you effectively unsubscribe from all its groups of mail.
    """
    set_subscription(session, customer_or_group.id, False)
    customer_or_group.is_subscribed = False


def set_subscription(session, id_number, subscribe_flag):
    """ Subscribe or unsubscribe to a customer or group. Set subscribe_flag to True or False. """
    url = '{uri}/0/groups/{group_id}?subscription={subscription}'.format(
        uri=session.uri,
        group_id=id_number,
        subscription=('true' if subscribe_flag else 'false')
    )
    session.put(url)
    if subscribe_flag:
        logging.info('Subscribed to ' + id_number)
    else:
        logging.info('Unsubscribed from ' + id_number)


def get_categories(session, segment):
    """ Get the categories that customers are divided into. Returns a list of Catagory instances.

    Set segment to Segment.public to get a list with all categories for government entities. Set it to Segment.private
    to get a list with all categories for private companies.
    """
    url = '{uri}/0/groups/segment/{segment}'.format(uri=session.uri, segment=segment.value)
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')
    categories = []
    for category_xml in body.find('categories'):
        category = Category()
        category.id = category_xml['categoryid']
        category.name = category_xml['name']
        category.customer_amount = int(category_xml['customers'])
        category.segment = segment
        categories.append(category)
    return categories


def get_from_category(session, category):
    """ Get a list of customers in a specific category. """
    url = '{uri}/0/groups/category/{category_id}'.format(uri=session.uri, category_id=category.id)
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')
    return extract_customers_from_xml(body)


def get_subscribed(session, segment):
    """ Get all customers that you have subscribed with.

    Set segment to Segment.public to get a list with government entities. Set it to Segment.private to get a list
    with private companies.

    This list includes customers that you have only partially subscribed to, by subscribing to only some of its mail
    groups.
    """
    url = '{uri}/0/groups/subscriptions?list={segment}'.format(uri=session.uri, segment=segment.value)
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')
    return extract_customers_from_xml(body)


def extract_customers_from_xml(xml):
    customers = []
    for customer_xml in xml.find('customers'):
        customer = Customer()
        customer.id = customer_xml['custgrpid']
        customer.name = customer_xml['name']
        customer.logo_url = customer_xml.logo.string
        customer.is_subscribed = int_string_to_bool(customer_xml['subscription'])
        customers.append(customer)
    return customers


def int_string_to_bool(string):
    if string == '1':
        return True
    elif string == '0':
        return False
    raise ValueError()
