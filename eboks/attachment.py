import copy
import bs4


class Attachment:
    """ Class describing an attachment to an e-boks message. """
    def __init__(self):

        # Unique attachment identification number. The id cannot change.
        self.id = ''

        # The name of the attachment.
        self.name = ''

        # The file type of the attachment, can be any 1-4 letter string (pdf, txt, html, exe, etc.).
        self.filetype = ''

        # The size in bytes of the attachment.
        self.size_bytes = 0

        # A copy of the message.
        self.message = None


def get_attachments(session, message):
    """ Get a list of attachments that a message has. """
    url = '{uri}/0/mail/folder/{folder_id}/message/{message_id}'.format(
        uri=session.uri,
        folder_id=message.folder_id,
        message_id=message.id
    )
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')

    if not body.message.attachements:
        return []

    # The xml body has a lot of information on the message. Some of it is ids, names, filetypes and sizes of the
    # attachments. We convert these to a list of Attachment instances.
    attachments = []
    for attachment_xml in body.message.attachements:
        attachment = Attachment()
        attachment.id = attachment_xml['id']
        attachment.name = attachment_xml['name']
        attachment.filetype = attachment_xml['format']
        attachment.size_bytes = int(attachment_xml['size'])
        attachment.message = copy.deepcopy(message)
        attachments.append(attachment)
    return attachments


def get_content(session, attachment):
    """ Get attachment content as a string.

    The requests library has tried to choose the best decoding option. """
    return _get_content_impl(session, attachment).text


def get_content_bytes(session, attachment):
    """ Get attachment content as a raw byte array. """
    return _get_content_impl(session, attachment).content


def _get_content_impl(session, attachment):
    """ Get a response object containing an attachments content. """
    url = '{uri}/0/mail/folder/{folder_id}/message/{message_id}/content'.format(
        uri=session.uri,
        folder_id=attachment.message.folder_id,
        message_id=attachment.id
    )
    response = session.get(url)
    return response
