import bs4


class Sender:
    """ A sender is someone that can send you mail on e-boks. """
    def __init__(self):

        # Unique sender identification number. The id cannot change. It is possible to send mail to yourself, by
        # e.g uploading documents to e-boks. The sender id for yourself is '0'.
        self.id = ''

        # The name of the sender.
        self.name = ''

        # Amount of unread message from the sender.
        self.total_unread = 0

        # An url to the senders logo.
        self.logo_url = ''


def get_myself():
    """ Get a sender that represent yourself. Use for example to fetch messages uploaded by you. """
    me = Sender()
    me.id = '0'
    me.name = '(Own document)'
    me.total_unread = 0
    me.logo_url = ''
    return me


def get_senders(session):
    """ Get a list of senders that have sent you atleast one mail. """
    url = '{uri}/0/mail/folders/search/senders'.format(uri=session.uri)
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')

    # Convert the response body to a list of senders.
    senders = []
    for sender_xml in body.searchfolders:
        sender = Sender()
        sender.name = sender_xml['name']
        sender.total_unread = int(sender_xml['unread'])
        sender.id = sender_xml.sender['id']
        sender.logo_url = sender_xml.sender['image']
        senders.append(sender)
    return senders


def get_message_total(session, sender):
    """ Get the total amount of messages that you have from a specific sender.

    Set sender to eboks.sender.myself() to get the amount of messages uploaded by you.
    """
    url = '{uri}/0/mail/folder/search/senders/{sender_id}?skip=0&take=0'.format(uri=session.uri, sender_id=sender.id)
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')
    return int(body.searchfolder['messages'])
