import eboks

# Logon will ask for your cpr, password and activation code.
session = eboks.EboksSession()
session.logon()

# Fetch the list of folders.
folders = eboks.folder.get_folders(session)
print()
print("Your eboks folders are:")
for folder in folders:
    print(folder.id, folder.name, sep=': ')
    
# Find inbox folder.
inbox = None
for folder in folders:
    if folder.type == eboks.folder.Type.inbox:
        inbox = folder

# Fetch the list of messages in inbox.
print()
print("Your messages in Inbox are:")
for message in eboks.message.get_from_folder(session, inbox):
    print(message.id, message.name, sep=': ')
    
session.logoff()

