# PostPython

Python binding for the e-Boks rest API used by the official mobile 
applications.

/eboks - the python module.
/test_eboks.py - unit-tests for the python module.
/example.py - small example showing how to use the module.
/investigator.py - gui program for looking at e-Boks queries.

# Required Python Libraries

requests
beautifulsoup4
python-dateutil